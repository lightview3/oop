/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class Sharp extends Filter{
    public Sharp(){
        super();
        this.filtername = "Резкость";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image){
        float[] sharpKernel = {
     0.0f, -1.0f, 0.0f,
     -1.0f, 5.0f, -1.0f,
     0.0f, -1.0f, 0.0f
 };
 BufferedImageOp sharpen = new ConvolveOp(
     new Kernel(3, 3, sharpKernel),
     ConvolveOp.EDGE_NO_OP, null);
        BufferedImage result = sharpen.filter(image, null);
        return result;
    }


}
