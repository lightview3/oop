/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.LookupOp;
import java.awt.image.ShortLookupTable;

public class Posterize extends Filter{
    public Posterize(){
        super();
        this.filtername = "32тыс цветов";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image){
        short[] posterize = new short[256];
 for (int i = 0; i < 256; i++)
   posterize[i] = (short)(i - (i % 32));
 BufferedImageOp posterizeOp =
     new LookupOp(new ShortLookupTable(0, posterize), null);
        BufferedImage result = posterizeOp.filter(image, null);
        return result;
    }


}