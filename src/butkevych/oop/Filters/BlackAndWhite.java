/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;

/**
 *
 * @author Vova
 */
public class BlackAndWhite extends Filter{
    public BlackAndWhite(){
        super();
        this.filtername = "Черно-Белый";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image){
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorConvertOp op = new ColorConvertOp(cs, null);
        BufferedImage result = op.filter(image, null);
        cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        op = new ColorConvertOp(cs, null);
        result = op.filter(result, null);
        return result;
    }


}
