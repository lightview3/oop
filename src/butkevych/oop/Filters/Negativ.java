/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import butkevych.oop.Utils;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.LookupOp;
import java.awt.image.ShortLookupTable;

/**
 *
 * @author Vova
 */
public class Negativ extends Filter{
    public Negativ(){
        super();
        this.filtername = "Негатив";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image) {
        short[] invert = new short[256];
        for (int i = 0; i < 256; i++) {
            invert[i] = (short) (255 - i);
        }
        BufferedImageOp invertOp = new LookupOp(
                new ShortLookupTable(0, invert), null);
        BufferedImage result = invertOp.filter(image, null);
        return result;
    }


}