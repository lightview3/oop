/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import butkevych.oop.Utils;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.LookupOp;
import java.awt.image.ShortLookupTable;

/**
 *
 * @author Vova
 */
public class Sepia extends Filter{
    public Sepia(){
        super();
        this.filtername = "Сепия";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image) {
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorConvertOp op = new ColorConvertOp(cs, null);
        BufferedImage it1 = op.filter(image, null);
        cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        op = new ColorConvertOp(cs, null);
        it1 = op.filter(it1, null);
        short[] r = new short[256];
        short[] g = new short[256];
        short[] b = new short[256];
        for (int i = 0; i < 256; i++) {
            r[i] = (short) ((i+80)>255? 255: (i+80));
            g[i] = (short) ((i+40)>255? 255: (i+40));
            b[i] = (short) i;
        }
        short[][] sepiaInvert = new short[][]{r, g, b};
        BufferedImageOp sepiaInvertOp
                = new LookupOp(new ShortLookupTable(0, sepiaInvert), null);
        BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        result = sepiaInvertOp.filter(it1, result);
        return result;
    }

}
