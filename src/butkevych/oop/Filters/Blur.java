/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop.Filters;

import butkevych.oop.Filter;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class Blur extends Filter{
    public Blur(){
        super();
        this.filtername = "Размытие";
    }
    @Override
    public BufferedImage start_filter(BufferedImage image){
        float ninth = 1.0f / 9.0f;
        float[] blurKernel = {
            ninth, ninth, ninth,
            ninth, ninth, ninth,
            ninth, ninth, ninth
        };
        BufferedImageOp blur = new ConvolveOp(new Kernel(3, 3, blurKernel));
        BufferedImage result = blur.filter(image, null);
        return result;
    }


}
