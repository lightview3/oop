/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butkevych.oop;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 *
 * @author Vova
 */
public class Utils {
    public static int[] int_to_argb(int argb) {
        int[] result = new int[4];
        //get alpha
        result[0] = (argb >> 24) & 0xff;

        //get red
        result[1] = (argb >> 16) & 0xff;

        //get green
        result[2] = (argb >> 8) & 0xff;

        //get blue
        result[3] = argb & 0xff;

        return result;
    }
    
    
    public static List<String> getClasseNamesInPackage
     (String jarName, String packageName){
   ArrayList classes = new ArrayList ();

   packageName = packageName.replaceAll("\\." , "/");
   if (true) System.out.println
        ("Jar " + jarName + " looking for " + packageName);
   try{
     JarInputStream jarFile = new JarInputStream
        (new FileInputStream (jarName));
     JarEntry jarEntry;

     while(true) {
       jarEntry=jarFile.getNextJarEntry ();
       if(jarEntry == null){
         break;
       }
       if((jarEntry.getName ().startsWith (packageName)) &&
            (jarEntry.getName ().endsWith (".class")) ) {
         if (true) System.out.println
           ("Found " + jarEntry.getName().replaceAll("/", "\\."));
         classes.add (jarEntry.getName().replaceAll("/", "\\.").replace(".class", ""));
       }
     }
   }
   catch( IOException e){
     e.printStackTrace ();
   }
   return classes;
}
}
