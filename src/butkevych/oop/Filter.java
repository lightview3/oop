
package butkevych.oop;


import java.awt.image.BufferedImage;


public abstract class Filter {
    protected String filtername;
    public Filter(){
    }
    public BufferedImage start_filter(BufferedImage image){
        for(int i = 0; i < image.getWidth(); i++)
            for(int j = 0; j < image.getHeight(); j++){
                System.out.println(i+" "+j);
                int argb = image.getRGB(i,j);
                image.setRGB(i, j, argb);
            }
        return image;
    }
   // protected  abstract int edit_color(int argb);
    public String getFilterName(){
        return this.filtername;
    }
}
